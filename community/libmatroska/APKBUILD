# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Timo Teräs <timo.teras@iki.fi>
pkgname=libmatroska
pkgver=1.6.1
pkgrel=0
pkgdesc="Extensible multimedia container format based on EBML"
url="https://www.matroska.org"
arch="all"
options="!check"  # Tests don't build; 'make check' target disabled by upstream
license="LGPL-2.1-or-later"
makedepends="cmake libebml-dev>=1.3.9"
subpackages="$pkgname-dev"
source="https://dl.matroska.org/downloads/libmatroska/libmatroska-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		$CMAKE_CROSSOPTS
	make
}

package() {
	make install DESTDIR="$pkgdir"
}

sha512sums="fb852416ecb12787cd82d54271350646d9a8e4e1fc77efec648f51b4369ca69d777069538257a89d78d81e83a8e84243967264af6e2e66dba8154f4210d2e4a8  libmatroska-1.6.1.tar.xz"
