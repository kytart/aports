# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=libhandy1
pkgver=0.85.0
pkgrel=0
pkgdesc="Library full of GTK+ widgets for mobile phones"
url="https://gitlab.gnome.org/GNOME/libhandy"
arch="all !s390x !mips !mips64" # Limited by 'ibus'
license="LGPL-2.0-or-later"
makedepends="meson glib-dev gtk+3.0-dev vala py3-setuptools gobject-introspection-dev
	glade-dev"
checkdepends="xvfb-run ibus adwaita-icon-theme"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.gnome.org/sources/libhandy/${pkgver%.*}/libhandy-$pkgver.tar.xz
	"
builddir="$srcdir/libhandy-$pkgver"

build() {
	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=plain \
		-Dexamples=true \
		. output
	ninja -C output
}

check() {
	xvfb-run ninja -C output test
}

package() {
	DESTDIR="$pkgdir" ninja -C output install
}

dev() {
	default_dev

	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr
}

sha512sums="f92c277db689db7bb6eeba98fee8024158033f05c44eaf973522ac09f7967d7dd5e5c6ca29e5dbd561ec80f989d767f84cbbac3c74937e91c03ed55874a1dc3d  libhandy-0.85.0.tar.xz"
